/*******************************************************************************
################################################################################
   Project Name : LoRa Car Parking node with Smart Sleep
################################################################################
   Copyright (c) 2019 Sooraj V S, Ajmi A S,Thomas Telkamp and Matthijs Kooijman

   Permission is hereby granted, free of charge, to anyone
   obtaining a copy of this document and accompanying files,
   to do whatever they want with them without any restriction,
   including, but not limited to, copying, modification and redistribution.
   NO WARRANTY OF ANY KIND IS PROVIDED.

   ToDo:
   - set NWKSKEY
   - set APPKSKEY
   - set DEVADDR
   - optionally comment #define DEBUG
   - optionally comment #define SLEEP
   - change mydata to another (small) static text
   - set SLEEP_PERIOD in seconds (will be readjusted based on sleep logic)
   - set TX_INTERVAL 
   - set CALIBRATION_INTERVAL 

*******************************************************************************/

#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <Wire.h>
#include <MechaQMC5883.h>
#include <LowPower.h>

MechaQMC5883 qmc;

//DEVICE CONFIGURATION
static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const u4_t DEVADDR = 0x00000000 ; // <-- Change this address for every node!


// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

// show debug statements; comment next line to disable debug statements
#define DEBUG

// use low power sleep; comment next line to not use low power sleep
#define SLEEP

static osjob_t sendjob;
long avg_x, avg_y, avg_z;
bool tx_ready;
bool next_transmission;
bool vehicle_flag;
bool deep_sleep_flag;
bool calib;
int sleeps_before_tx = 0;
int sleeps_before_calibration = 0;
int rms = 0;
int timer=0;
int timer1x=0;
int timer2x=0;
int timer3x=0;
int timer4x=0;
int timer5x=0;
int timer6x=0;
int timer7x=0;
int timer8x=0;
int timer9x=0;
int timer10x=0;
int timer_temp=0;
int timer1x_temp = 0;
int timer2x_temp = 0;
int timer3x_temp = 0;
int timer4x_temp = 0;
int timer5x_temp = 0;
int timer6x_temp = 0;
int timer7x_temp = 0;
int timer8x_temp = 0;
int timer9x_temp = 0;
int timer10x_temp = 0;

struct {
  int rms_val;
  char vehicle_presence;
  int voltage;
  int sleep_interval;
  int minitues;
  char error;
} mydata;

bool sendTransmission = 0;
unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change:
const long interval = 1000;             // interval at which to blink (milliseconds)

unsigned int SLEEP_PERIOD = 8;                         //* Multiples of 8 in Seconds

#define MAXIMUM_SLEEP_LIMIT   600                         //  Multiples of 8 in seconds (10min)
#define SLEEP_INCREMENT       8                          //* Multiples of 8 in seconds (1min)
#define CALIBRATION_INTERVAL  48                         //*  In Hours
#define TX_INTERVAL           5                          //* Number of sleeps required for single transmission if there is any state change

const lmic_pinmap lmic_pins = {
  .nss = 6,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {2, 3, 4},
};

void onEvent (ev_t ev) {

  switch (ev) {
    case EV_TXCOMPLETE:
    #ifndef SLEEP
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
    #else
      next_transmission = true;
    #endif
    break;
  }
}

void do_send(osjob_t* j) {

  if (LMIC.opmode & OP_TXRXPEND)
    {
      #ifdef DEBUG
      Serial.println(F("OP_TXRXPEND, not sending"));
      #endif
    }
  else
    {
      LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
      #ifdef DEBUG
      Serial.println(F(" ...Packet Sent..."));
      #endif
    }
  tx_ready = 0;
}

void(* resetFunc) (void) = 0; //declare reset function @ address 0

void setup() {

  pinMode(7, OUTPUT); //Connected to a Turn ON/OFF Sensor
  Serial.begin(9600);
  Wire.begin();


  #ifdef DEBUG
  Serial.println(" ");
  Serial.println("################################################################");
  Serial.println("....Car Parking Node with Self Calibation & Deep Sleep....");
  Serial.println("################################################################");
  delay(1000);
  #endif

  calibration();

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  #ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  #else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
  #endif

  #if defined(CFG_eu868)
  // Set up the channels
  //used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  // NA-US channels 0-71 are configured automatically

  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band

  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.
  #elif defined(CFG_us915)
  // NA-US channels 0-71 are configured automatically
  // but only one group of 8 should (a subband) should be active
  // TTN recommends the second sub band, 1 in a zero based count.
  // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
  LMIC_selectSubBand(1);
  #endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  // LMIC.dn2Dr = DR_SF10;

  // Set data rate and tx_ready power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);

  // Start job
  do_send(&sendjob);
  vehicle_flag = false;

}

void calibration() {

  int x, y, z;
  int sample_count = 10;
  int count = 0;
  long sum_x = 0, sum_y = 0, sum_z = 0;


  digitalWrite(7, HIGH);
  delay(100);
  Wire.begin();
  qmc.init();
  delay(100);
  
  #ifdef DEBUG
  Serial.println(" ");
  Serial.println("#########################################");
  Serial.println("....Calibration Started....");
  Serial.println("#########################################");
  Serial.println(" ");
  #endif
  

  while (count < sample_count)
    {
      qmc.read(&x, &y, &z);
      delay(300);
      sum_x += x;
      sum_y += y;
      sum_z += z;
    #ifdef DEBUG
      Serial.print(count);
      Serial.print(":x- ");
      Serial.print(x);
      Serial.print(", y- ");
      Serial.print(y);
      Serial.print(", z- ");
      Serial.println(z);
    #endif
      count ++;
    }

  avg_x = (sum_x / sample_count);
  avg_y = (sum_y / sample_count);
  avg_z = (sum_z / sample_count);

  #ifdef DEBUG
    Serial.println(" ");
    Serial.println("#########################################");
    Serial.println("....Average....");
    Serial.println("#########################################");
    Serial.print("x: ");
    Serial.print(avg_x);
    Serial.print(", y: ");
    Serial.print(avg_y);
    Serial.print(", z: ");
    Serial.println(avg_z);
    Serial.println("#########################################");
    Serial.println("....Calibration Complete....");
    Serial.println("#########################################");
    Serial.println(" ");
  #endif
  calib=true;
  readSensor();
  Volt();  
  delay(100);
  digitalWrite(7, HIGH);
  delay(100);
}

void readSensor() {

  int x, y, z;
  int rms_temp = 0;
  int rms_temp_loop = 0;
  int presence = 0;

  digitalWrite(7, HIGH);
  delay(100);
  Wire.begin();
  qmc.init();
  delay(100);

  qmc.read(&x, &y, &z);

  x = x - avg_x;
  y = y - avg_y;
  z = z - avg_z;

  #ifdef DEBUG
  Serial.println("#########################################");
  Serial.print(" x: ");
  Serial.print(x);
  Serial.print(", y: ");
  Serial.print(y);
  Serial.print(", z: ");
  Serial.println(z);
  #endif

  rms = sqrt(square(x) + square(y) + square(z));

  if (rms > 125)
  {
    int count = 0;
    while (count < 5)
      {
        qmc.read(&x, &y, &z);
        x = x - avg_x;
        y = y - avg_y;
        z = z - avg_z;
        rms_temp_loop += sqrt(square(x) + square(y) + square(z));
        count++;
      }
    rms_temp = rms_temp_loop / 5;
    rms = rms_temp;
  }


    mydata.rms_val=rms;
     #ifdef DEBUG
    Serial.print(" RMS: ");
    Serial.println(rms);
    #endif


//vehicle confirmation  
  if (rms_temp > 125)
  {
    mydata.vehicle_presence = 1;
    #ifdef DEBUG
    Serial.println("####...Vehicle Present..####");
    #endif
  }
  else
  {
    mydata.vehicle_presence = 0;
    #ifdef DEBUG
    Serial.println("####...Slot Empty...####");
    #endif
    }

    
//state change detection
  if (rms > 125 && vehicle_flag == false)
  {
    vehicle_flag = true;
    tx_ready = 1;
    deep_sleep_flag = false;
  }

  if (rms <= 125 && vehicle_flag == true)
  {
    vehicle_flag = false;
    tx_ready = 1;
    deep_sleep_flag = false;
  }


//deep sleep
  if(sleeps_before_tx == TX_INTERVAL)
  {
    deep_sleep_flag = true;  
    
    if(SLEEP_PERIOD < MAXIMUM_SLEEP_LIMIT)
    {
    SLEEP_PERIOD += SLEEP_INCREMENT; 
    }

    tx_ready = 1;   
    sleeps_before_tx = 0; 
  }


//transmission
  if (tx_ready == 1)
  {
    next_transmission = false;
    os_setCallback(&sendjob, do_send);
    sleeps_before_tx = 0;
    
    if(deep_sleep_flag == false)
      {
      SLEEP_PERIOD = SLEEP_INCREMENT;
      }

  }


//Software Timer

if(SLEEP_PERIOD == SLEEP_INCREMENT)
{
 timer1x_temp++;
 timer1x = timer1x_temp;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 2)
{
 timer2x_temp++;
 timer2x = (timer2x_temp*2)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 3)
{
 timer3x_temp++;
 timer3x = (timer3x_temp*3)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 4)
{
 timer4x_temp++;
 timer4x = (timer4x_temp*4)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 5)
{
 timer5x_temp++;
 timer5x = (timer5x_temp*5)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 6)
{
 timer6x_temp++;
 timer6x = (timer6x_temp*6)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 7)
{
 timer7x_temp++;
 timer7x = (timer7x_temp*7)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 8)
{
 timer8x_temp++;
 timer8x = (timer8x_temp*8)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 9)
{
 timer9x_temp++;
 timer9x = (timer9x_temp*9)-1;
}
else if(SLEEP_PERIOD == SLEEP_INCREMENT * 10)
{
 timer10x_temp++;
 timer10x = (timer10x_temp*10)-1;
}


  timer = (timer1x + timer2x + timer3x  + timer4x + timer5x + timer6x + timer7x  + timer8x  + timer9x + timer10x)-1;
  mydata.minitues = timer;

  #ifdef DEBUG
    Serial.println("#########################################");
    Serial.print(" Working for : ");
    Serial.print(timer);
    Serial.println(" Minitues");
    Serial.println("#########################################");
  #endif

    
//calibration
  if (vehicle_flag == false && timer > (CALIBRATION_INTERVAL * 60)-1)
    {
     Serial.flush(); // give the serial print chance to complete
     resetFunc();  //call reset
    }
  
  if (calib==true)
    {
   mydata.sleep_interval = 0;
   calib=false;
    }
    else{
    mydata.sleep_interval = SLEEP_PERIOD;
    }
  
  digitalWrite(7, LOW);
  delay(100);
}

void Volt()
{
  double analogvalue = analogRead(A3);
  double bat_temp = ((analogvalue * 3.3) / 1024); //ADC voltage*Ref. Voltage/1024
  double sum = 0;
  double avg = 0;

  for (byte  i = 0; i < 4; i++)
  {
    sum += bat_temp * 2;
  }
  avg = (sum / 4);
  mydata.voltage = avg * 100;

  #ifdef DEBUG
    Serial.print(" Battery Voltage = ");
    Serial.print(avg);
    Serial.println("V");
    Serial.println("#########################################");
  #endif
}

void loop() {
    
  int sleepcycles = SLEEP_PERIOD / 8;  // calculate the number of sleepcycles (8s) given the TX_INTERVAL
  extern volatile unsigned long timer0_overflow_count;
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)

  {
    previousMillis = currentMillis;
  }
  os_runloop_once();

  #ifndef SLEEP
  os_runloop_once();
  #else



  if (next_transmission == false) {
    os_runloop_once();
  }
    
  else {
    #ifdef DEBUG
    Serial.print(F(" Enter sleeping for "));
    Serial.print(sleepcycles);
    Serial.println(F(" cycles of 8 seconds"));
    Serial.println("#########################################");
    #endif

    Serial.flush(); // give the serial print chance to complete

    for (int i = 0; i < sleepcycles; i++)
    {
      // Enter power down state for 8 s with ADC and BOD module disabled
      LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
      //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
      // LMIC uses micros() to keep track of the duty cycle, so
      // hack timer0_overflow for a rude adjustment:
      cli();
      timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
      sei();
    }
    sleeps_before_tx++;
    sleeps_before_calibration++;

  #ifdef DEBUG
    Serial.println(" ");
    Serial.println("#########################################");
    Serial.println(F("Sleep complete"));
    Serial.println("#########################################");
    Serial.print(" Sleep Count : ");
    Serial.println(sleeps_before_tx);
    Serial.print(" Calibration Count : ");
    Serial.println(sleeps_before_calibration);
  #endif
    
    readSensor();
    Volt();

  }
#endif
}
